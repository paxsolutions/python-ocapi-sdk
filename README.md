# Python module for OCAPI (under development)

## How to use

### Prerequisites

- [OCAPI settings](https://github.com/SalesforceCommerceCloud/ocapi-settings) must be added to the instance under test.
- pip install requirements.txt
- [OCAPI Docs](https://documentation.b2c.commercecloud.salesforce.com/DOC1/topic/com.demandware.dochelp/OCAPI/current/usage/OpenCommerceAPI.html?cp=0_15)

### Authentication with OCAPI can happen one of two ways

You can create a file named `.pycapi` at the root of the repo using the following format (Not tested on Windows).

```shell
[default]
client_id = <CLIENT_ID>
client_secret = <CLIENT_SECRET>
hostname = <INSTANCE_URI>
api_version = v20_9
```

or

Manually supply credentials through an API instance.

```python
from ocapi.client import ShopAPI

api = ShopAPI(hostname='<INSTANCE_URI>', client_id='<CLIENT_ID>', client_secret='<CLIENT_SECRET>', api_version='v20_9')

api.product_search(site_id='<SITE_ID>', query='<CATEGORY>')
```

### **Note: The above uses a very limited portion of the search endpoint [docs](https://documentation.b2c.commercecloud.salesforce.com/DOC1/topic/com.demandware.dochelp/OCAPI/current/shop/Resources/ProductSearch.html). In order to fully utilize the search API, the product_search method will need to be improved.**


### Output should be:

```shell
[{'_type': 'product_search_hit',
  'hit_type': 'master',
  'link': 'https://<INSTANCE_URI>/s/<SITE_ID>/dw/shop/v20_4/products/188882C01?q=rings&client_id=cea04f38-4d79-4a1d-b3cb-171b771dccce',
  'product_id': '188882C01',
  'product_name': 'A Great Product',
  'product_type': {'_type': 'product_type', 'master': True},
  'represented_product': {'_type': 'product_ref',
   'id': '188882C01-48',
   'link': 'https://<INSTANCE_URI>/s/<SITE_ID>/dw/shop/v20_4/products/188882C01-48?q=rings&client_id=cea04f38-4d79-4a1d-b3cb-171b771dccce'}}],
...
```

### From here we can parse and compile a product URL...

---
All code authored by: Erik Marty